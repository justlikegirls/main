/**
 * Created by Администратор on 20.04.2015.
 */
public class AI {

    private final int SHIP = 1;

    private Cell[] randomCells;
    private int randomIndex = 0;

    private final int MIX_COUNT = 300;

    public AI(){
        randomCells = getRandomCells();
    }

    private Cell[] getRandomCells(){
        Cell []randomCells = new Cell[GameModel.FIELD_WIDTH * GameModel.FIELD_HEIGHT];
        for(int i = 0, cellIndex = 0; i < GameModel.FIELD_WIDTH; i++){
            for(int j = 0; j < GameModel.FIELD_WIDTH; j++, cellIndex++)
                randomCells[cellIndex] = new Cell(i, j);
        }

        mixCells(randomCells, MIX_COUNT);
        return randomCells;
    }

    private void mixCells(Cell[] cells, int rounds){
        for(int roundsPassed = 0; roundsPassed < rounds; roundsPassed++) {
            for (int i = 0; i < cells.length; i++) {
                int randomIndex = (int)(Math.random() * cells.length);
                    swap(i, randomIndex, cells);
            }
        }
    }

    private void swap(int i, int j, Cell[] cells){
        int tempRow = cells[j].row;
        int tempCol = cells[j].col;
        cells[j].col = cells[i].col;
        cells[j].row = cells[i].row;
        cells[i].row = tempRow;
        cells[i].col = tempCol;
    }

    public Ships generateShips(){
        int[][] field = new int[GameModel.FIELD_HEIGHT][GameModel.FIELD_WIDTH];
        Ships ships = new Ships();

        Ship fourBlockShip = getRandomShip(Ship.SHIP_SIZE.FOUR, field);
        ships.addShip(fourBlockShip);

        for(int i = 0; i < 2; i++){
            Ship threeBlockShip = getRandomShip(Ship.SHIP_SIZE.THREE, field);
            ships.addShip(threeBlockShip);
        }

        for(int i = 0; i < 3; i++){
            Ship twoBlockShip = getRandomShip(Ship.SHIP_SIZE.TWO, field);
            ships.addShip(twoBlockShip);
        }

        for(int i = 0; i < 4; i++){
            Ship oneBlockShip = getRandomShip(Ship.SHIP_SIZE.ONE, field);
            ships.addShip(oneBlockShip);
        }

        return ships;
    }

    private void markShip(Ship ship, int[][] field){
        for(int i = 0; i < ship.getHeight(); i++)
            for(int j = 0; j < ship.getWidth(); j++)
                field[i + ship.getStartRow()][j + ship.getStartColumn()] = SHIP;
    }

    private Ship getRandomShip(Ship.SHIP_SIZE size, int[][] field){
        Ship randomShip;
        boolean canLocate = false;

        do {
            int i = (int) Math.round(Math.random() * GameModel.FIELD_HEIGHT);
            int j = (int) Math.round(Math.random() * GameModel.FIELD_WIDTH);
            Ship.SHIP_ROTATION rotation = Math.random() > 0.5 ? Ship.SHIP_ROTATION.ZERO : Ship.SHIP_ROTATION.NINETY;
            randomShip = new Ship(rotation, size, i, j);
            boolean isValid = randomShip.isValid();
            if(isValid){
                canLocate = haveNoCollision(randomShip, field);
            }
        } while(!canLocate);

        markShip(randomShip, field);

        return randomShip;
    }

    private boolean haveNoCollision(Ship ship, int[][] field){
        boolean noCollisions = true;

        for(int i = 0; i < ship.getHeight(); i++) {
            for (int j = 0; j < ship.getWidth(); j++){
                if(field[i + ship.getStartRow()][j + ship.getStartColumn()] == SHIP){
                    noCollisions = false;
                    break;
                }
            }
        }

        return noCollisions;
    }

    public Cell makeChoice(){
        Cell randomCell = randomCells[randomIndex];
        randomIndex++;
        return randomCell;
    }
}
