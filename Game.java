import javax.swing.*;

/**
 * Created by Администратор on 20.04.2015.
 */
public class Game extends JPanel {

    private GameModel model;
    private GameView view;
    private GameController controller;
    private AI ai;

    public Game(Ships playerShips){
        ai = new AI();
        Ships aiShips = ai.generateShips();
        model = new GameModel(playerShips, aiShips);
        controller = new GameController(model, ai);
        view = new GameView(model, controller);
        add(view);
    }
}
