import javax.swing.*;
import java.awt.*;

/**
 * Created by Администратор on 19.04.2015.
 */
public class GameView extends JPanel{

    private GameModel model;
    private GameController controller;

    private GameField playerField;
    private GameField aiField;

    public GameView(GameModel model, GameController controller){
        this.model = model;
        model.addEventListener(new ModelChangedListener() {
            @Override
            public void modelChanged(ModelChangedEvent e) {
                parseModelChange(e);
            }
        });

        this.controller = controller;

        playerField = getGameField("Player field", model.getPlayerField(), " ", "[+]", "O", "[-]");
        add(playerField);

        aiField = getGameField("AI field", model.getAIField(), " ", "[+]", "O", "[-]");
        aiField.addFieldCellClickedListener(this.controller);
        add(aiField);
    }

    private GameField getGameField(String fieldTitle, GameModel.FIELD_VALUE[][] values,
                                   String emptyCell, String shipCell, String missCell, String shootedCell){
        return new GameField(fieldTitle, values, emptyCell, shipCell, missCell, shootedCell);
    }

    private void aiWin(){
        showMessage("AI WIN!");
    }

    private void showMessage(String message){
        removeAll();
        JLabel label = new JLabel(message);
        label.setAlignmentY(CENTER_ALIGNMENT);
        add(label);
        updateUI();
    }

    private void playerWin(){
        showMessage("PLAYER WIN!");
    }

    private void parseModelChange(ModelChangedEvent e){
        int i = e.getRowNumber();
        int j = e.getColumnNumber();

        if (model.isPlayersTurn) {
            aiField.setFieldState(i, j, model.getAIField()[i][j]);
            int aiShipCellsCount = model.getAIShipCellsCount();
            if(aiShipCellsCount == 0)
                playerWin();
        }else {
            playerField.setFieldState(i, j, model.getPlayerField()[i][j]);
            int playerShipCellsCount = model.getPlayerShipCellsCount();
            if(playerShipCellsCount == 0)
                aiWin();
        }
    }
}
