import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Администратор on 20.04.2015.
 */
public class CreateShipsPanel extends JPanel{

    private JButton createOneBlockShipBtn;
    private JButton createTwoBlockShipBtn;
    private JButton createThreeBlockShipBtn;
    private JButton createFourBlockShipBtn;

    private Ships ships;

    private JRadioButton ninetyDegreeRotationRbtn;
    private JRadioButton zeroDegreeRotationRbtn;
    private ButtonGroup rBtnGroup;

    private TableWithLabels table;

    private JTextField iTextField;
    private JTextField jTextField;

    private ShipsCreatedDispatcher dispatcher;

    private final String SHIP_CELL = "[+]";

    private JLabel messageLabel;

    private final int STRUT_HEIGHT = 10;

    private final int BUTTON_WIDTH = 200;
    private final int BUTTON_HEIGHT = 30;

    private final int TEXT_FIELD_WIDTH = 50;
    private final int TEXT_FIELD_HEIGHT = 25;

    public CreateShipsPanel(){
        setLayout(new FlowLayout(FlowLayout.CENTER));
        ships = new Ships();
        createTable();

        Box commonBox = Box.createVerticalBox();
        initButtons();
        addButtons(commonBox);
        addButtonListeners();
        initTextFields();
        addTextFields(commonBox);
        initRadioButtons();
        addRadioButtons(commonBox);
        initMessageLabel();
        addMessageLabel(commonBox);
        addDispatcher();
        add(commonBox);
    }

    private void initMessageLabel(){
        messageLabel = new JLabel("Системные сообщения");
    }

    private void addMessageLabel(Box commonBox){
        commonBox.add(Box.createVerticalStrut(4 * STRUT_HEIGHT));
        commonBox.add(messageLabel);
    }

    public void addEventListener(ShipsCreatedListener listener){
        dispatcher.addEventListener(listener);
    }

    public void removeEventListener(ShipsCreatedListener listener){
        dispatcher.removeEventListener(listener);
    }

    private void addDispatcher(){
        dispatcher = new ShipsCreatedDispatcher();
    }

    private void initTextFields(){
        iTextField = new JTextField("0");
        iTextField.setMaximumSize(new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT));
        iTextField.setPreferredSize(new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT));

        jTextField = new JTextField("0");
        jTextField.setMaximumSize(new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT));
        jTextField.setPreferredSize(new Dimension(TEXT_FIELD_WIDTH, TEXT_FIELD_HEIGHT));
    }

    private void addTextFields(Box commonBox){
        final int STRUT_WIDTH = 10;
        Box textBoxes = Box.createHorizontalBox();
        textBoxes.add(Box.createHorizontalStrut(80));
        textBoxes.add(new JLabel("y:"));
        textBoxes.add(Box.createHorizontalStrut(STRUT_WIDTH));
        textBoxes.add(iTextField);
        textBoxes.add(Box.createHorizontalStrut(STRUT_WIDTH));
        textBoxes.add(new JLabel("x:"));
        textBoxes.add(Box.createHorizontalStrut(STRUT_WIDTH));
        textBoxes.add(jTextField);
        commonBox.add(textBoxes);
        commonBox.add(Box.createVerticalStrut(3 * STRUT_HEIGHT));
    }

    private void createTable(){
        JTable notEditableTable = new NotEditableTable(new DefaultTableModel(GameModel.FIELD_WIDTH, GameModel.FIELD_HEIGHT), 40, 40);
        table = new TableWithLabels(notEditableTable);
        add(table);
    }

    private void addButtons(Box commonBox){
        commonBox.add(createOneBlockShipBtn);
        commonBox.add(Box.createVerticalStrut(STRUT_HEIGHT));
        commonBox.add(createTwoBlockShipBtn);
        commonBox.add(Box.createVerticalStrut(STRUT_HEIGHT));
        commonBox.add(createThreeBlockShipBtn);
        commonBox.add(Box.createVerticalStrut(STRUT_HEIGHT));
        commonBox.add(createFourBlockShipBtn);
        commonBox.add(Box.createVerticalStrut(3 * STRUT_HEIGHT));
    }

    private void addRadioButtons(Box commonBox){
        commonBox.add(zeroDegreeRotationRbtn);
        commonBox.add(Box.createVerticalStrut(STRUT_HEIGHT));
        commonBox.add(ninetyDegreeRotationRbtn);
        commonBox.add(Box.createVerticalStrut(STRUT_HEIGHT));
    }

    private void initRadioButtons(){
        ninetyDegreeRotationRbtn = new JRadioButton("Ninety degree rotation");
        zeroDegreeRotationRbtn = new JRadioButton("Zero degree rotation");
        zeroDegreeRotationRbtn.setSelected(true);
        rBtnGroup = new ButtonGroup();
        rBtnGroup.add(ninetyDegreeRotationRbtn);
        rBtnGroup.add(zeroDegreeRotationRbtn);
    }

    private Ship.SHIP_ROTATION getShipRotation(){
        Ship.SHIP_ROTATION rotation = zeroDegreeRotationRbtn.isSelected() ? Ship.SHIP_ROTATION.ZERO : Ship.SHIP_ROTATION.NINETY;
        return rotation;
    }

    private void createOneBlockShip(){
        createShip(Ship.SHIP_SIZE.ONE);
    }

    private void createTwoBlockShip(){
        createShip(Ship.SHIP_SIZE.TWO);
    }

    private void createThreeBlockShip(){
        createShip(Ship.SHIP_SIZE.THREE);
    }

    private void createFourBlockShip(){
        createShip(Ship.SHIP_SIZE.FOUR);
    }

    private void createShip(Ship.SHIP_SIZE size){
        Ship.SHIP_ROTATION rotation = getShipRotation();

        int i;
        int j;
        try {
            i = Integer.valueOf(iTextField.getText());
            j = Integer.valueOf(jTextField.getText());
        }
        catch (Exception e){
            iTextField.setText("0");
            iTextField.setText("0");
            messageLabel.setText("Введены некорректные координаты!");
            return;
        }

        Ship newShip = new Ship(rotation, size, i, j);
        if(newShip.isValid()) {
            if (haveNoCollision(newShip)) {
                if (ships.addShip(newShip)) {
                        drawShip(newShip);
                        messageLabel.setText("Корабль поставлен успешно!");
                        if (ships.allShipsLocated())
                            dispatcher.dispatchEvent(new ShipsCreatedEvent(this, ships));
                }else {
                    messageLabel.setText("Лимит этих кораблей исчерпан!");
                }
            }else
            messageLabel.setText("Здесь уже стоит корабль!");
        }
        else{
            messageLabel.setText("Корабль выходит за границу!");
        }
    }

    private boolean haveNoCollision(Ship ship){
        boolean noCollisions = true;

        for(int i = 0; i < ship.getHeight(); i++) {
            for (int j = 0; j < ship.getWidth(); j++) {
                String tableValue = (String) table.getValueAt(ship.getStartRow() + i, ship.getStartColumn() + j);
                if(tableValue == SHIP_CELL){
                    noCollisions = false;
                    break;
                }
            }
        }

        return noCollisions;
    }

    private void drawShip(Ship ship){
        for(int i = 0; i < ship.getHeight(); i++) {
            for (int j = 0; j < ship.getWidth(); j++) {
                table.setValueAt(SHIP_CELL, ship.getStartRow() + i, ship.getStartColumn() + j);
            }
        }
    }

    private void addButtonListeners(){
        createOneBlockShipBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createOneBlockShip();
            }
        });

        createTwoBlockShipBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createTwoBlockShip();
            }
        });

        createThreeBlockShipBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createThreeBlockShip();
            }
        });

        createFourBlockShipBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createFourBlockShip();
            }
        });
    }

    private void initButtons(){
        createOneBlockShipBtn = getButton("Create One Block ship");
        add(createOneBlockShipBtn);

        createTwoBlockShipBtn = getButton("Create Two Block ship");
        add(createTwoBlockShipBtn);

        createThreeBlockShipBtn = getButton("Create Three Block ship");
        add(createThreeBlockShipBtn);

        createFourBlockShipBtn = getButton("Create Four Block ship");
        add(createFourBlockShipBtn);
    }

    private JButton getButton(String text){
        JButton button = new JButton(text);
        button.setMaximumSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
        button.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
        return button;
    }
}
