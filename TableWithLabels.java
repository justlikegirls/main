import javax.swing.*;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.peer.LightweightPeer;

/**
 * Created by Администратор on 20.04.2015.
 */
public class TableWithLabels extends JPanel{

    private JTable table;

    private final int TOP_STEP = 20;
    private final int LEFT_STEP = 20;

    public TableWithLabels(JTable table){
        this.table = table;
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        setBorder(BorderFactory.createEmptyBorder(TOP_STEP, LEFT_STEP, 0, 0));
        add(table);
    }

    public synchronized void addMouseListener(MouseListener l) {
       table.addMouseListener(l);
    }

    public synchronized void removeMouseListener(MouseListener l) {
        table.removeMouseListener(l);
    }

    public int rowAtPoint(Point point) {
        return table.rowAtPoint(point);
    }

    public int columnAtPoint(Point point) {
        return table.columnAtPoint(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        drawTopAxe(g);
        drawLeftAxe(g);
    }

    private void drawLeftAxe(Graphics g){
        int stepY = table.getRowHeight();
        for(int i = 0; i < table.getColumnCount(); i++){
            g.drawString(Integer.toString(i), LEFT_STEP - 15, TOP_STEP + stepY / 2 + i * stepY);
        }
    }

    private void drawTopAxe(Graphics g){
        TableColumnModel columnModel = table.getColumnModel();
        int stepX = columnModel.getColumn(0).getWidth();
        for (int i = 0; i < columnModel.getColumnCount(); i++)
            g.drawString(Integer.toString(i), LEFT_STEP + stepX / 2 + i * stepX, TOP_STEP - 5);

    }

    public Object getValueAt(int row, int column) {
        return table.getValueAt(row, column);
    }

    public void setValueAt(Object aValue, int row, int column) {
        table.setValueAt(aValue, row, column);
    }
}
