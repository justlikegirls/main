import javax.swing.*;

/**
 * Created by Администратор on 20.04.2015.
 */
public class MainApplet extends JApplet{
    private static CreateShipsPanel createShipsPanel;

    private static Game game;

    public void init(){
        createShipsPanel = new CreateShipsPanel();
        add(createShipsPanel);
        createShipsPanel.addEventListener(new ShipsCreatedListener() {
            @Override
            public void shipsCreated(ShipsCreatedEvent e) {
                createGame(e.getShips());
            }
        });
    }

    private void createGame(Ships playerShips){
        remove(createShipsPanel);
        game = new Game(playerShips);
        add(game);
        repaint();
        game.updateUI();
    }
}
