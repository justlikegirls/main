import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

/**
 * Created by Администратор on 19.04.2015.
 */
public class NotEditableTable extends JTable{

    public NotEditableTable(TableModel tableModel, int cellWidth, int cellHeight){
        super(tableModel);
        setCellSelectionEnabled(false);
        setFocusable(false);
        TableColumnModel model = getColumnModel();
        setCellAlignment();
        for(int i = 0; i < model.getColumnCount(); i++){
            model.getColumn(i).setPreferredWidth(cellWidth);
            model.getColumn(i).setMaxWidth(cellWidth);
        }

        this.setRowHeight(cellHeight);
    }

    private void setCellAlignment(){
        for (int i = 0; i < getModel().getRowCount(); i++) {
            for (int j = 0; j < getModel().getColumnCount(); j++) {
                DefaultTableCellRenderer renderer =
                        (DefaultTableCellRenderer)this.getCellRenderer(i, j);
                renderer.setHorizontalAlignment(JTextField.CENTER);
            }
        }
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        return false;
    }
}
